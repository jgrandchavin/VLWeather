# VLWEATHER API 

## SETUP
* Cloner le repo 
* Dupliquer le `.env.example` en le renomer en `.env` et mettre les variables selon votre configuration
* Démarrer l'API avec soit `nodemon` ou soit `node app.js`
* Pour acceder à la documentation de l'API aller sur le lien de l'API avec un navigateur et rajouter `/swagger-ui` (Example: `localhost:3000/swagger-ui`)
* Faites les différents init et populate de la partie `Setup 

## Description de l'API
##### Fonctionne avec 2 bases de donnée:
* MySQL pour les places de décollage
* Mongo pour stocker les données météos

##### Avec l'API vous pourrez:
* Créer/supprimer des lieux de décollage
* Savoir si les conditions d'un lieu donné sont "volable" ou non 

## Comportement de l'API:
Lorsque vous chercheriez à savoir si un lieu est volable ou non, l'API va aller chercher dans la base Mongo si la requête n'a pas déjà été faite moins de 30 min avant.
* Si c'est le cas, l'API va reprendre les données de la dernière recherche qui a été enregistré en base et tester si c'est "volable" ou pas 
* Sinon l'API va faire un call à l'API d'Openweather, va tester si c'est "volable" ou pas et enregirster les données météo dans la base Mongo
