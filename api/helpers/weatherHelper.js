convertDegreeInDirection = function(windDegree) {
    let windDirection = null;
    if (windDegree < 11.25) {
        windDirection = "N";
    } else if (windDegree < 33.75) {
        windDirection = "NNE";
    }	else if (windDegree < 56.25) {
        windDirection = "NE";
    }	else if (windDegree < 78.75) {
        windDirection = "ENE";
    }	else if (windDegree < 101.25) {
        windDirection = "E";
    }	else if (windDegree < 123.75) {
        windDirection = "ESE";
    }	else if (windDegree < 146.25) {
        windDirection = "SE";
    }	else if (windDegree < 168.75) {
        windDirection = "SSE";
    }	else if (windDegree < 191.25) {
        windDirection = "S";
    }	else if (windDegree < 213.75) {
        windDirection = "SSO";
    }	else if (windDegree < 236.25) {
        windDirection = "SO";
    }	else if (windDegree < 258.75) {
        windDirection = "OSO";
    }	else if (windDegree < 281.25) {
        windDirection = "O";
    }	else if (windDegree < 303.75) {
        windDirection = "ONO";
    }	else if (windDegree < 326.25) {
        windDirection = "NO";
    }	else if (windDegree < 348.75) {
        windDirection = "NNO";
    } else {
        windDirection = "N";
    }

    return windDirection;
};

module.exports.isFlyingPossible = function(weather, windSpeed, windDirection, windDirectionAllowedByTakeOffPlace) {

    let errors = [];
    let response = {};

    /****** TEST WIND SPEED ******/
    // Convert wind speed in KM/H
    windSpeed = windSpeed * 3.6;
    if (windSpeed > 35) {
        errors.push('wind speed');
    }

    /****** TEST WIND DIRECTION ******/
    // Convert wind direction in direction
    windDirection = convertDegreeInDirection(windDirection);
    if (!windDirectionAllowedByTakeOffPlace.includes(windDirection) && !windDirectionAllowedByTakeOffPlace.includes('TOUTES')) {
        errors.push('bad wind direction');
    }

    /****** TEST WEATHER ******/

    if (weather === 'Rain' || weather === 'Snow' ) {
        errors.push('bad weather');
    }

    if (errors.length <= 0 ){
        response = {status: true, message: 'You can fly!'}
    }
    else {
        let  message = "You can't fly because of ";
        let isFirst = true;
        errors.forEach(function(element){
            if (isFirst) {
                isFirst = false;
                message += element;
            } else {
                message += ', ' + element;
            }
        });
        response = {status: false, message: message}
    }

    return response;
};