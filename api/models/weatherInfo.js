const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const weatherInfoSchema = new Schema({
    takeoff_place_id: {
        type: String
    },
    wind_speed: {
        type: Number
    },
    wind_direction : {
        type: Number
    },
    weather: {
        type: String
    },
    date : {
        type: Date,
        default: Date.now
    }
}, { usePushEach: true });

const WeatherInfo = module.exports = mongoose.model('WeatherInfo', weatherInfoSchema);

module.exports.createWeatherInfo = function (newWeatherInfo, callback) {
    newWeatherInfo.save(callback);
};

