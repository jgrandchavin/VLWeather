const router = require('express').Router();

router.use('/setup', require('./setup'));
router.use('/weather', require('./weather'));
router.use('/takeoffplace', require('./takeoffplace'));

module.exports = router ;