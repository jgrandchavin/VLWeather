const router = require('express').Router();

router.use('/', require('./setup'));

module.exports = router ;