const router = require('express').Router();
const mySQL = require('../../../config/config').mySQL;
const WeatherInfo = require('../../models/weatherInfo');
const takeOffPlacesData = require('../../static/data/decollages.json');

// INIT
router.get('/mysql/init', (req, res) => {

    console.log("--- Init mySQL database ---");

    // DROP TABLE IF EXISTS
    mySQL.query("DROP TABLE IF EXISTS " + process.env.MYSQL_TABLENAME + ";", function(err){
       if (err) {
           console.log("[ERROR] Failed to drop mySQL table");
           res.json({status: false, message: 'Failed to drop table'});
       }
       else {

           console.log("[INFO] MySQL table " + process.env.MYSQL_TABLENAME + " dropped");

           // CREATE TABLE
           mySQL.query("CREATE TABLE " + process.env.MYSQL_TABLENAME + "(id int NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), lat FLOAT, lon FLOAT, winddirection VARCHAR(255));", function (err) {
               if (err) {
                   console.log("[ERROR] Failed to create mySQL table");
                   res.json({status: false, message: 'Failed to create table'});
               }
               else {
                   console.log("[INFO] MySQL table " + process.env.MYSQL_TABLENAME + " created");
                   res.json({status: true, message: 'Init successful'});
               }
           })
       }
    });
});

// POPULATE WITH TAKE-OFF PLACES
router.get('/mysql/populate', (req, res) => {
    console.log("--- Populate MySQL ---");
    takeOffPlacesData.forEach(function(element) {

        let directionArray = '';
        let isFirst = true;
        if (element.orientations != undefined) {

            element.orientations.orientation.forEach(function(element){
                if (isFirst) {
                    isFirst = false;
                    directionArray += element._value;
                } else {
                    directionArray += "," + element._value;
                }
            });

        }
        const query = 'INSERT INTO ' + process.env.MYSQL_TABLENAME + ' (name, lat, lon, winddirection) VALUES ("' + element.nom + '", ' + element.coord._lat + ', ' + element.coord._lon + ',"' + directionArray + '");';
        mySQL.query(query, function(err){
            if (err) {
                console.log("[ERROR] Failed to insert value -> " + query );
            }
        });
    });
    console.log("[INFO] Populate done");
    res.json({status: true, message: 'Populate done'});
});

router.get('/mongodb/init', (req,res) => {
    console.log("--- Init MongoDB ---");
    WeatherInfo.remove({}, function(err) {
        if (err) {
            console.log("[INFO] Can't dropped weatherInfo collection")
        }
        console.log('[INFO] Collection weatherInfo dropped');
    });
    res.json({status: true, message: 'MongoDB init done'})
});

module.exports = router;