const router = require('express').Router();
const mySQL = require('../../../config/config').mySQL;

router.post('/', (req, res) => {
    let newtakoffPlace = {
        name: req.body.name,
        lat: req.body.lat,
        lon: req.body.lon,
        winddirection: req.body.winddirection
    };

    const query =  'INSERT INTO ' + process.env.MYSQL_TABLENAME + ' (name, lat, lon, winddirection) VALUES ("' + newtakoffPlace.name + '", ' + newtakoffPlace.lat + ', ' + newtakoffPlace.lon + ',"' + newtakoffPlace.winddirection + '");';

    mySQL.query(query, function(err){
        if (err) {
            console.log("[ERROR] Create take-off place failed");
            res.json({status: false, message:"Create take-off place failed"})
        }
        else {
            console.log("[Info] Create take-off place successful");
            res.json({status: true, message:"Create take-off place successful"})
        }
    });
});


router.delete('/delete', (req, res) => {
    const takeoffPlaceId = req.query.takeoffplaceid;

    mySQL.query("DELETE FROM " + process.env.MYSQL_TABLENAME + " WHERE id=" + takeoffPlaceId + ";", (err, resp) => {
        if (err) {
            console.log("[INFO] Can't delete value");
            res.json({status: false, message: 'Delete value failed'});
        } else {
            console.log("[INFO] Delete value successful");
            res.json({status: true, message: 'Delete value successful'});
        }
    })
});


module.exports = router;