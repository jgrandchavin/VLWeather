const router = require('express').Router();
const request = require('request');
const weatherHelper = require('../../helpers/weatherHelper');
const takeOffPlacesData = require('../../static/data/decollages.json');
const mySQL = require('../../../config/config').mySQL;
const WeatherInfo = require('../../models/weatherInfo');


router.get('/is-flying-possible', (req, res) => {
    const takeoffPlaceId = req.query.takeoffplaceid;

    let takeoffPlaceLat = 0;
    let takeoffPlaceLon = 0;
    let takeoffPlaceWindDirectionAllowed = [];

    // GO SEARCH THE LOCATION IN MYSQL DB
    mySQL.query("SELECT * FROM takeoffplaces WHERE ID=" + takeoffPlaceId +";", function(err, response) {
        if (response.length <= 0) {
            res.json({status: false, message: 'No take-off place with this ID'})
        } else {
            takeoffPlaceLat = response[0].lat;
            takeoffPlaceLon = response[0].lon;
            takeoffPlaceWindDirectionAllowed = response[0].winddirection.split(",");

            let recentData = [];

            console.log("--- Test if flying is possible ---");
            // // IF THERE IS VALUE LESS THAN 30MIN AGO FOR THE TAKE-OFF PLACE
            const date = new Date(Date.now());
            date.setMinutes(date.getMinutes()-30);
            WeatherInfo.find({takeoff_place_id: takeoffPlaceId, date: {$gt: date }}, function (err, docs) {
                recentData = docs;

                // IF THERE IS VALUE LESS THAN 30MIN AGO FOR THE TAKE-OFF PLACE
                if (recentData.length > 0) {
                    WeatherInfo.findOne({takeoff_place_id: takeoffPlaceId}, function (err, doc) {
                        console.log("[INFO] Search take-off place's weather info in MONGODB database");
                        let testResponse = weatherHelper.isFlyingPossible(doc.weather, doc.wind_speed, doc.wind_direction, takeoffPlaceWindDirectionAllowed);
                        res.json(testResponse);
                    }).sort({date: -1});
                    // IF THERE IS NOT VALUE LESS THAN 30MIN AGO FOR THE TAKE-OFF PLACE
                } else {
                    request('http://api.openweathermap.org/data/2.5/weather?lat=' + takeoffPlaceLat + '&lon=' + takeoffPlaceLon + '&appid=' + process.env.OPENWEATHER_API_TOKEN, {json: true}, (err, ress, response) => {
                        if (err) {
                            console.log("[ERROR] Can't have weather condition")
                        }

                        let newWeatherInfo = WeatherInfo ({
                            takeoff_place_id: takeoffPlaceId,
                            wind_speed: response.wind.speed,
                            wind_direction: response.wind.deg,
                            weather: response.weather[0].main
                        });

                        WeatherInfo.createWeatherInfo(newWeatherInfo, (err, _) => {
                            if (err) {
                                console.log("[ERROR] Can't put the info in MONGODB ");
                            }
                            console.log("[INFO] Take-off place's weather info stored in MONGODB database")
                        });

                        // TEST IF THE TAKEOFF PLACE IS FLYABLE
                        let testResponse = weatherHelper.isFlyingPossible(response.weather[0].main, response.wind.speed, response.wind.deg, takeoffPlaceWindDirectionAllowed);
                        res.json(testResponse);
                    });
                }
            });
        }
    });
});

module.exports = router;