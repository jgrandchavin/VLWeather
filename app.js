// Required constants
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express'), swaggerDocument = require('./api/plugins/swagger/swagger.json');
require('./config');

// Initialized API
const app = express();
const server = require('http').Server(app);

app.use(bodyParser.json());
//app.use(morgan('dev'));

// Port
const port = process.env.API_PORT || 3000 ;

// Route
app.use('/swagger-ui', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api', require('./api'));

// Start Server
server.listen(port, function () {
    console.log(`[INFO] Server started on port ${port}`);
});
