const router = require('express').Router();
const mongoose = require('mongoose');
const mysql = require('mysql');
const dotEnv = require('dotenv');

dotEnv.config();

// MySQL config
const mysqlConnection = mysql.createConnection({
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DBNAME
});

mysqlConnection.connect(function(err) {
    if(err){
        console.log("[ERROR] Couldn't connect to MySQL database");
    }
    else {
        console.log("[INFO] Connected to MySQL database")
    }
});

// MONGODB Config
mongoose.connection.openUri('mongodb://' + process.env.MONGODB_HOST + ':' + process.env.MONGODB_PORT + '/' + process.env.MONGODB_DBNAME);

mongoose.connection.on('connected', () => {
    console.log("[INFO] Connected to MongoDB database");
});

mongoose.connection.on('error', () => {
    console.log("[INFO] Couldn't connected to MongoDB database");
});

module.exports = router;
module.exports.mySQL = mysqlConnection;