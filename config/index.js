const router = require('express').Router();

router.use('/', require('./config'));

module.exports = router ;